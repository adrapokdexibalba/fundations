<?php
class Beneficios extends ActiveRecord
{
	/**
	 * Retirna los beneficios disponibles
	 * 
	 */
	public function getBeneficios($page, $ppage=20) {
		return $this->paginate("page: $page", "per_pae: $ppage", "order: id desc");
	}
}