<?php 
/**
 * Cargamos el modelo de los beneficios
 */
	//Load::models('beneficios');// En la V1 se autocargan los modelos
	/**
	* 
	*/
	class BeneficiosController extends AppController
	{
		
		public function index($page=1)
		{
			$beneficio = new Beneficios();
			$this->listBeneficios = $beneficio->getBeneficios($page);
		}


    /**
     * Crea un Beneficio
     */
    public function create ()
    {
        /**
         * Se verifica si el usuario envio el form (submit) y si ademas 
         * dentro del array POST existe uno llamado "Beneficio"
         * el cual aplica la autocarga de objeto para guardar los 
         * datos enviado por POST utilizando autocarga de objeto
         */
        if(Input::hasPost('beneficio')){
            /**
             * se le pasa al modelo por constructor los datos del form y ActiveRecord recoge esos datos
             * y los asocia al campo correspondiente siempre y cuando se utilice la convención
             * model.campo
             */
            $beneficio = new Beneficios(Input::post('beneficio'));
            //En caso que falle la operación de guardar
            if($beneficio->create()){
                Flash::valid('Operación exitosa');
                //Eliminamos el POST, si no queremos que se vean en el form
                Input::delete();
                return;               
            }else{
                Flash::error('Falló Operación');
            }
        }
    }
 
    /**
     * Edita un Registro
     *
     * @param int $id (requerido)
     */
    public function edit($id)
    {
        $beneficio = new Beneficios();
        //se verifica si se ha enviado el formulario (submit)
        if(Input::hasPost('beneficio')){
 			$beneficio->find_first($id);

            if($beneficio->update(Input::post('beneficio'))){
                 Flash::valid('Operación exitosa');
                //enrutando por defecto al index del controller
                return Redirect::to();
            } else {
                Flash::error('Falló Operación');
            }
        } else {
            //Aplicando la autocarga de objeto, para comenzar la edición
            $this->beneficio = $beneficio->find_by_id((int)$id);
        }
    }
 
    /**
     * Eliminar un beneficio
     * 
     * @param int $id (requerido)
     */
    public function del($id)
    {
        $beneficio = new Beneficios();
        if ($beneficio->delete((int)$id)) {
                Flash::valid('Operación exitosa');
        }else{
                Flash::error('Falló Operación'); 
        }
 
        //enrutando por defecto al index del controller
        return Redirect::to();
    }
	}